envMap:
  # See https://raw.githubusercontent.com/firefly-iii/firefly-iii/main/.env.example
  APP_URL: "https://{{ .Values.ingress.main.host }}"
  APP_ENV: "local"
  APP_DEBUG: "false"
  SITE_OWNER: "mail@example.com"
  DEFAULT_LANGUAGE: en_US
  DB_HOST: "{{ .Release.Name }}-mariadb"
  DB_PORT: "3306"
  DB_DATABASE: "firefly"
  DB_USERNAME: "firefly"
  DB_CONNECTION: "mysql"
  VANITY_URL: "https://{{ .Values.ingress.main.host }}"
  FIREFLY_III_URL: "http://{{ .Release.Name }}"
  STATIC_CRON_TOKEN: "Fh7cQH2N3ED8yr9aCk5BxJsU6pPYjLXt"
  TRUSTED_PROXIES: "*"
  CAN_POST_AUTOIMPORT: "true"
  IMPORT_DIR_ALLOWLIST: "/import"

env:
  - name: DB_PASSWORD
    valueFrom:
      secretKeyRef:
        name: mariadb
        key: mariadb-password
  - name: APP_KEY
    valueFrom:
      secretKeyRef:
        name: firefly
        key: appkey
  - name: AUTO_IMPORT_SECRET
    valueFrom:
      secretKeyRef:
        name: firefly
        key: autoimport-secret

image:
  repository: fireflyiii/core
  tag: latest

controller:
  enabled: true
  type: Deployment
  replicas: 1
  ports:
    - name: http
      containerPort: 8080
      protocol: TCP

cronjobs:
  - name: cron
    schedule: "0 3 * * *"
    command: "wget -qO- http://{{ .Release.Name }}/api/v1/cron/{{ .Values.envMap.STATIC_CRON_TOKEN }}"
  - name: auto-import
    schedule: "0 8,20 * * *"
    image: curlimages/curl
    env:
      - name: AUTOIMPORT_SECRET
        valueFrom:
          secretKeyRef:
            name: firefly
            key: autoimport-secret
    command: curl --location --request POST 'http://{{ .Release.Name }}-importer/autoimport?directory=/import&secret='"$AUTOIMPORT_SECRET"

extraContainers:
  # - name: cron
  #   image: alpine:latest
  #   command:
  #     - sh
  #     - -c
  #   args:
  #     - echo "0 3 * * * wget -qO- http://{{ .Release.Name }}/api/v1/cron/{{ .Values.envMap.STATIC_CRON_TOKEN }}" | crontab - && crond -f -L /dev/stdout

service:
  enabled: true
  type: ClusterIP
  ports:
    - name: http
      port: 80
      protocol: TCP
      targetPort: http

ingress:
  main:
    enabled: false
    host: firefly.example.com
    tls:
      enabled: true
      issuer: cloudflare-dns
    entrypoints:
      - lansecure

persistence:
  defaultStorageClass: freenas-nfs-csi
  data:
    enabled: true
    size: 2Gi
    storageClass: freenas-iscsi-csi
    mountPath: /var/www/html/storage/upload

generatedSecrets:
  # name is tpl
  - name: mariadb
    forceRegenerate: false
    data:
    fields:
      - fieldName: "mariadb-password"
        length: "32"
      - fieldName: "mariadb-root-password"
        length: "32"
  - name: firefly
    forceRegenerate: false
    data:
    fields:
      - fieldName: "appkey"
        length: "32"
      - fieldName: autoimport-secret
        length: "32"

externalSecrets:
  # - secretStore:
  #     kind: ClusterSecretStore
  #     name: external-secrets
  #   # tpl
  #   targetName: importer-config
  #   data:
  #     - secretKey: bank1.json
  #       remoteRef:
  #         key: BANK1_CONFIG

importer:
  enabled: true
  image:
    repository: fireflyiii/data-importer
    tag: latest
  ports:
    - name: http
      containerPort: 8080
      protocol: TCP
  service:
    enabled: true
    type: ClusterIP
  ingress:
    enabled: false
    host: firefly-importer.example.com
    tls:
      enabled: true
      issuer: cloudflare-dns
    entrypoints:
      - lansecure
  # Refs to secret containing autoimport configs
  # autoImportSecret: importer-config

mariadb:
  auth:
    database: firefly
    username: firefly
    existingSecret: mariadb
  primary:
    persistence:
      enabled: true
      size: 2Gi
      storageClass: freenas-iscsi-csi
